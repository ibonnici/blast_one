// Find the relevant columns from the header line,
// and figure the columns used in the process.
// The header is not expected to be ascii,
// so it'll be checked for utf-8 correctness.

use std::{
    collections::{HashMap, HashSet},
    str,
};

use arrayvec::ArrayVec;

use crate::{
    columns::{ColumnsSkips, MAX_N_COLS},
    config::{ColumnsNames, ColumnsUsed},
    errors::{errout, Error},
};

pub(crate) fn parse_header(
    input: &[u8],
    colnames: &ColumnsNames,
    used: Option<ColumnsUsed>, // Can be provided to ignore columns present in the header.
    separator: u8,             // Guaranteed ASCII byte by config checker.
) -> Result<ColumnsSkips, Error> {
    let input = str::from_utf8(input)
        .map_err(|e| Error::HeaderParse(format!("Header line is not valid utf-8: {e}")))?;
    let separator = [separator];
    let separator = str::from_utf8(&separator).unwrap();

    // Expect blast style.
    if !input.starts_with('#') {
        errout!(HeaderParse(
            "Header line did not start with leading '#' character.".into()
        ));
    }
    let input = &input[1..]; // Skip leading hash.

    // Only focus on names useful in the next.
    let relevant_names: HashSet<_> = if let Some(used) = used {
        used.expected_order(colnames).into_iter().collect()
    } else {
        colnames.iter().collect()
    };

    // Collect names from the header line,
    // along with their absolute column position.
    let input_names = input.split(separator);
    let mut collected = HashMap::with_capacity(MAX_N_COLS); // {name ↦ i_col}
    for (i_col, name) in input_names.enumerate() {
        if !relevant_names.contains(name) {
            continue;
        }
        if let Some(i_previous) = collected.get(name) {
            let (prev, curr) = (i_previous + 1, i_col + 1); // Humans count from 1.
            errout!(HeaderParse(format!(
                "Column name {name:?} specified twice in header: \
                 once in column {prev} and once in column {curr}."
            )));
        }
        collected.insert(name, i_col);
    }

    if collected.is_empty() {
        errout!(HeaderParse(format!(
            "Could not find any relevant column in header line with separator {separator:?}."
        )));
    }

    // Check that all required names have been found.
    // And that they are in the right order.
    let mut collected = collected.into_iter().collect::<ArrayVec<_, MAX_N_COLS>>();
    collected.sort_unstable_by_key(|&(_, i_col)| i_col);
    let Some(order) = ColumnsUsed::new(&collected.iter().map(|&(name, _)| name), colnames) else {
        let colsep = "  ";
        let actual = collected
            .into_iter()
            .map(|(name, i_col)| {
                let i_col = i_col + 1; // Humans count from 1.
                format!("{i_col}: {name:?}")
            })
            .collect::<Vec<_>>()
            .join(colsep);
        let expected = if let Some(used) = used {
            used.expected_order(colnames)
                .into_iter()
                .map(|name| format!("{name:?}"))
                .collect::<Vec<_>>()
                .join(colsep)
        } else {
            let mut expected = ColumnsUsed::iter()
                .map(|c| {
                    c.expected_order(colnames)
                        .into_iter()
                        .map(|name| format!("{name:?}"))
                        .collect::<Vec<_>>()
                        .join(colsep)
                })
                .take(2);
            let alt1 = expected.next().unwrap();
            let alt2 = expected.next().unwrap();
            format!("{alt1}\nOr alternately:\n  {alt2}")
        };
        errout!(HeaderParse(format!(
            "Found relevant columns in this order based on the header line:\n  {actual}\n\
             But the program requires the following columns in this order:\n  {expected}\n\
             If both column {:?} and the couple ({:?}, {:?}) are given, \
             then both will be used and compared for consistency.",
            colnames.btop, colnames.query_sequence, colnames.subject_sequence,
        )));
    };

    // Ready to calculate skips.
    let skips = collected.into_iter().scan(0, |i_previous, (_, i_col)| {
        let skip = i_col - *i_previous;
        *i_previous = i_col + 1;
        Some(skip)
    });
    Ok(ColumnsSkips::new(order, skips))
}

// Convenience to iterate over every user-defined column name.
impl ColumnsNames {
    pub(crate) fn iter(&self) -> impl Iterator<Item = &str> {
        [
            &self.id,
            &self.query_start,
            &self.query_end,
            &self.btop,
            &self.query_sequence,
            &self.subject_sequence,
        ]
        .map(String::as_str)
        .into_iter()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[allow(clippy::too_many_lines)]
    fn check() {
        use ColumnsUsed as C;
        use Error as E;

        let colnames = ColumnsNames::default();

        // Ease "ArrayVec literal".
        macro_rules! arv {
            ($($it:expr),+) => {{
                let mut arv = ArrayVec::new();
                $( arv.push($it); )+
                arv
            }};
        }
        let success = |input_order, header: &str, output_order, skips: ArrayVec<_, MAX_N_COLS>| {
            println!("Expecting success while parsing header:\n  {header:?} with {input_order:?}");
            let actual = parse_header(header.as_bytes(), &colnames, input_order, b' ').unwrap();
            let expected = ColumnsSkips::new(output_order, skips.into_iter());
            assert_eq!(expected, actual);
        };

        // Simplest header.
        success(
            None,
            "#id qstart qend qseq sseq",
            C::IdQstartQendQseqSseq,
            arv![0, 0, 0, 0, 0],
        );
        // Introduce unused columns here and there.
        success(
            None,
            "#A id B qstart C qend D qseq E sseq F",
            C::IdQstartQendQseqSseq,
            arv![1, 1, 1, 1, 1],
        );
        success(
            None,
            "#id A qstart B B qend C qseq D D D sseq E",
            C::IdQstartQendQseqSseq,
            arv![0, 1, 2, 1, 3],
        );
        success(
            None,
            "#A A id B qstart C qend qseq D sseq E",
            C::IdQstartQendQseqSseq,
            arv![2, 1, 1, 0, 1],
        );
        // Only use columns required in input order, even though they appear in the header.
        success(
            Some(C::IdQstartQendBtop),
            "#id qstart qend qseq sseq btop",
            C::IdQstartQendBtop,
            arv![0, 0, 0, 2],
        );
        success(
            Some(C::IdQstartQendQseqSseq),
            "#id qstart qend qseq sseq btop",
            C::IdQstartQendQseqSseq,
            arv![0, 0, 0, 0, 0],
        );
        success(
            Some(C::IdQstartQendQseqSseq),
            "#id qstart qend btop qseq sseq",
            C::IdQstartQendQseqSseq,
            arv![0, 0, 0, 1, 0],
        );

        let failure = |input_order, header: &str, message: &str| {
            println!("Expecting failure while parsing header:\n  {header:?}");
            let actual = parse_header(header.as_bytes(), &colnames, input_order, b' ');
            match actual {
                Ok(skips) => panic!("Unexpected success: yielded {skips:?}."),
                Err(E::HeaderParse(actual)) => {
                    if actual != message {
                        println!(
                            "/!\\ Expected:\n>>>\n{message}\n<<<\n\
                                       Got:\n>>>\n{actual}\n<<<"
                        );
                        panic!("Unexpected error message.");
                    }
                }
                Err(e) => panic!("Unexpected error type: {e:?}."),
            };
        };
        let order_message = |variable_part: &str| {
            format!(
                "Found relevant columns in this order based on the header line:\n  \
                 {variable_part}\n\
                 But the program requires the following columns in this order:\n  \
                 \"id\"  \"qstart\"  \"qend\"  \"btop\"\n\
                 Or alternately:\n  \
                 \"id\"  \"qstart\"  \"qend\"  \"qseq\"  \"sseq\"\n\
                 If both column \"btop\" and the couple (\"qseq\", \"sseq\") are given, \
                 then both will be used and compared for consistency."
            )
        };

        // Missing leading dash.
        failure(
            None,
            "id qstart qend qseq sseq",
            "Header line did not start with leading '#' character.",
        );
        // Redundant column name.
        failure(
            None,
            "#id qstart qend qseq sseq qend",
            "Column name \"qend\" specified twice in header: \
             once in column 3 and once in column 6.",
        );
        failure(
            None,
            "#A id B qstart C qend D qseq E sseq F qend G",
            "Column name \"qend\" specified twice in header: \
             once in column 6 and once in column 12.",
        );
        // Wrong column separator.
        failure(
            None,
            "#id-qstart-qend-qseq-sseq",
            "Could not find any relevant column in header line with separator \" \".",
        );
        // Missing column name(s).
        failure(
            None,
            "#id qstart qseq sseq",
            &order_message("1: \"id\"  2: \"qstart\"  3: \"qseq\"  4: \"sseq\""),
        );
        failure(
            None,
            "#A B qseq C sseq D",
            &order_message("3: \"qseq\"  5: \"sseq\""),
        );
        // Wrong order.
        failure(
            None,
            "#id qstart qseq qend sseq",
            &order_message("1: \"id\"  2: \"qstart\"  3: \"qseq\"  4: \"qend\"  5: \"sseq\""),
        );
        failure(
            None,
            "#id qseq qend qstart sseq",
            &order_message("1: \"id\"  2: \"qseq\"  3: \"qend\"  4: \"qstart\"  5: \"sseq\""),
        );
        failure(
            None,
            "#A id B qseq C qend D qstart E sseq",
            &order_message("2: \"id\"  4: \"qseq\"  6: \"qend\"  8: \"qstart\"  10: \"sseq\""),
        );
    }
}
